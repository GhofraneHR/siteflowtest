import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tree-view';
  currentEvent = '';
  config = {
    showActionButtons: true,
    showAddButtons: true,
    showRenameButtons: true,
    showDeleteButtons: true,
    showRootActionButtons: true,
    enableExpandButtons: true,
    enableDragging: true,
    rootTitle: 'Company Tree',
    validationText: 'Enter valid company',
    minCharacterLength: 5,
    setItemsAsLinks: false,
    setFontSize: 32,
    setIconSize: 16
  };
  myTree = [
    {
      name: 'Exemple 1',
      id: 1,
      options: {
        hidden: false,
        position: 1,
      },
      childrens: [
        {
          name: 'Example 2',
          id: 2,
          childrens: []
        }
      ]
    },
    {
      name: 'Example 3',
      id: 3,
      childrens: [
        {
          name: 'Example 4',
          id: 4,
          childrens: []
        }
      ]
    }
  ];

  onDragStart(event) {
    this.currentEvent = ' on drag start';
    console.log(event);
  }
  onDrop(event) {
    this.currentEvent = 'on drop';
    console.log(event);
  }
  onAllowDrop(event) {
    this.currentEvent = 'on allow drop';
  }
  onDragEnter(event) {
    this.currentEvent = 'on drag enter';
  }
  onDragLeave(event) {
    this.currentEvent = 'on drag leave';
  }
  onAddItem(event) {
    this.currentEvent = 'on add item';
    console.log(event);
  }
  onStartRenameItem(event) {
    this.currentEvent = 'on start edit item';
  }
  onFinishRenameItem(event) {
    this.currentEvent = 'on finish edit item';
  }
  onStartDeleteItem(event) {
    console.log('start delete');
    this.currentEvent = 'on start delete item';
  }
  onFinishDeleteItem(event) {
    console.log('finish delete');
    this.currentEvent = 'on finish delete item';
  }
  onCancelDeleteItem(event) {
    console.log('cancel delete');
    this.currentEvent = 'on cancel delete item';
  }
}
