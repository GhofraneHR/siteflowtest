package siteflow.calculatorTest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/calc")
public class Calculator {
    @GET
    @Path("/add/{a}/{b}/{c}")
    public String addition(@PathParam("a") double a, @PathParam("b") double b, @PathParam("c") double c) {
        return "" + (a + b + c);
    }

    @GET
    @Path("/subtract/{a}/{b}/{c}")
    public String subtract(@PathParam("a") double a, @PathParam("b") double b, @PathParam("c") double c) {
        return "" + (a - b - c) ;
    }

    @GET
    @Path("/multiply/{a}/{b}/{c}")
    public String multiply(@PathParam("a") double a, @PathParam("b") double b, @PathParam("c") double c) {
        return ""+(a * b * c);
    }

    @GET
    @Path("/divide/{a}/{b}")
    public String divide(@PathParam("a") double a, @PathParam("b") double b) {
        return ""+(a / b);
    }

}
